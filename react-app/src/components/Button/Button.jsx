import React from "react";
import './Button.scss';

class Button extends React.Component {
  render() {
    return (
      <button className="button" onClick={this.props.handleChange}>
        {this.props.buttonText}
      </button>
    );
  }
}

export default Button;