import React from "react";
import Button from "../../components/Button/Button";

class ToggleUserInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "Stepan",
      age: 25,
      isActive: false,
    };
  }

  handleChange = () => {
    this.setState({
      isActive: !this.state.isActive,
    });
  };
  render() {
    return (
      <div className="content">
        {this.state.isActive && (
          <p>
            Name: {this.state.name}, age: {this.state.age}
          </p>
        )}
        <Button
          buttonText={this.state.isActive ? "Hide" : "Show"}
          handleChange={this.handleChange}
        />
      </div>
    );
  }
}

export default ToggleUserInfo;
