import React from "react";
import Button from "../../components/Button/Button";

class UserInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "Stepan",
      age: 25,
    };
  }

  handleChange = () => {
    this.setState({
      name: "Mykola",
      age: 30,
    });
  };

  render() {
    return (
      <div className="content">
        <p>
          Name: {this.state.name}, age: {this.state.age}
        </p>
        <Button buttonText={"Click on me"} handleChange={this.handleChange} />
      </div>
    );
  }
}

export default UserInfo;
