import './App.scss';
import UserInfo from './modules/UserInfo/UserInfo';
import ToggleUserInfo from './modules/ToggleUserInfo/ToggleUserInfo';

function App() {
  return (
    <div className='container'>
      <UserInfo/>
      <ToggleUserInfo/>
    </div>
  );
}

export default App;
